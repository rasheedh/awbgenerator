### Airwaybill Generation API and Testing UI

### How do I get set up? ###

Application setup has two part:

#### Setting up Python API part
* create virtualenv using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)
* change directory api folder
* pip3 install -r requirments.txt
* install [redis](http://redis.io/topics/quickstart) and start redis server (`redis-server`)

* Dependencies
    * Python 3.5
    * pip3
* Databases used
    * sqlite3
    * redis

#### Setting up angularjs UI part
* change directory to `ui` inside project folder
* `npm install`
* `bower install`


After API and UI setup is done, run `python3 app.py` from inside api folder
head over to `http://localhost:8080/#/login` to see the application running