'use strict';

angular.module('AwbApp')

.controller('MainController', 
    ['$rootScope', '$scope', '$timeout', 'ContentService', 'toaster',
    function($rootScope, $scope, $timeout, ContentService, toaster) {   
        // pagination
        $scope.totalItems = 100;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;

        $scope.setPage = function (pageNo) {
          $scope.currentPage = pageNo;
        };

        // user airwaybills list
        $scope.airwaybillsList = [];

        $scope.pageChanged = function() {
        ContentService.getUserAirwaybills($scope.currentPage, $scope.itemsPerPage).then(
            function (response) {
                $scope.airwaybillsList = response.data.airwaybills;
                $scope.totalItems = response.data.total_count;
            }
        );

        };
        $scope.pageChanged();
    
        // generate airwaybill
        $scope.airwaybillsCount = 1;
        $scope.generateAirwaybills = function () {
            ContentService.generateAirwaybills($scope.airwaybillsCount).then(
                function (response) {
                    var message_type = response.data.success ? 'success': 'error';
                    toaster.pop(message_type, message_type, response.data.message);
                },
                function (response) {
                    toaster.pop('error', "error", "Invalid input");
                }
            );

        };

        $scope.validationResultSet = {};
        $scope.validateAirwaybills = function () {
            if(!$scope.airwaybillsToValidate) {
                toaster.pop("error", "error", "Enter some airwaybills!")
                return false;
            }
            var awbs = $scope.airwaybillsToValidate.replace( /\n/g, " " ).split( " " );
            var cleaned_awbs = [];

            for(var i=0; i < awbs.length; i++) {
                if(awbs[i].length != 0) cleaned_awbs.push(awbs[i]);
            }
            ContentService.validateAirwaybills(cleaned_awbs.join()).then(
                function (response) {
                    $timeout(function(){
                        $rootScope.$apply(function(){
                            $scope.validationResultSet = response.data.result;
                        });
                    });
                },
                function (response) {
                    toaster.pop("error", "error", "Invalid input");
                }
            );
        };

    }]);
