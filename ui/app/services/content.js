'use strict';
 
angular.module('AwbApp')
 
.factory('ContentService',
    ['$http', '$cookieStore', '$rootScope', 
    function ($http, $cookieStore, $rootScope) {
        var service = {};

        var getToken = function () {
            var globals = $cookieStore.get('globals') || {};
            return globals.currentUser.token;
        };

        service.getUserAirwaybills = function (page, per_page) {
            var token = getToken();
            return $http.get('/awbs?page=' + page + '&per_page=' + per_page + '&token=' + token);
        };
 
        service.generateAirwaybills = function (count) {
            var token = getToken();
            return $http.post('/awbs/generate?count=' + count + '&token=' + token);            
        };
 
        service.validateAirwaybills = function (awbs) {
            var token = getToken();
            return $http.post('/awbs/validate?awbs=' + awbs + '&token=' + token);            
        };
 
        return service;
    }]);
